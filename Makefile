CC=g++
CFLAGS=-I.

main: main.o
	$(CC) -o busdatabuilder main.cpp

.PHONY: clean

clean:
	rm busdatabuilder main.o
