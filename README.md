# GTFS-PrevBusJsonGenerator

*(Google/General Transit Feed Specification)* https://developers.google.com/transit/gtfs/

**Why I Made This**

The problem is: If a vehicle has yet to start servicing a route, stop time estimates are not yet available, and schedule times are provided instead. (At least in Vancouver)
This is especially true at transit exchanges or any stop near the beginning of a route. There's no way to track the vehicle's location in real time.
By using this program's output, the preceeding scheduled activity of any bus route can easily be looked up, 
and apps can refer to this to determine what route to look for the vehicle in question- far in advance of the bus appearing on the route you wish to travel on.

Once implemented, my web app HyperBus will fetch the files to determine what busses to track for longer-sighted predictions.
https://ltnr.ca/bus.html?stopNo=52094

**Info About The Generated Files**

Each file represents one direction of a transit route (on a certain day). Inside: all trips for that route
and most importantly: what trip the vehicle did just before, the final stopCode, and what time the trip was supposed to end,
as well as `block_id` `"b":` and `trip_id` `"t":`.
This information is meant to be combined with real-time data from the transit company (TransLink in Vancouver, BC).

The output .json files only need to be recreated when the transit provider publishes a new GTFS dataset (about 3 months for Translink).

I will just be dumping them in a public folder which can be accessed freely at:
https://ltnr.ca/prevTrips/index.html (or directly for AJAX requests: `ltnr.ca/prevTrips/WK_{Rt#}_{StopID}.json`)

**How To Run It**

In short, you don't need to. Access the files hosted at the link above.

If you really want to, first be sure there is an empty folder "/prevTrips/" in the working directory (with all other project files).

Next, run the .bash script. It will download the lastest Translink data and after about 15 mins, it'll spit out all the .json files.

The script uses a program called "sendxmpp". If you don't have it, just comment all of it out, or change the recipient!

You can either download this repo, or clone it via git:

``` sh
git clone git@gitlab.com:colinner/gtfs-prevbusjsongenerator.git
cd gtfs-prevbusjsongenerator
make
```

Note that if you compile from source, you may need to change the executable name.
