#!/bin/bash

echo -n "BusDataBuilder is updating GTFS data" | sendxmpp --file /home/colin/.sendxmpprc --tls colinl@momi.ca

echo -e "\e[31m --------------- Fetching latest GTFS file: --------------- \e[0m"
wget --https-only --server-response --referer="HyperBus back-end at LTNR.ca" --no-cookies --output-document=google_transit.zip https://gtfs.translink.ca/static/latest


echo -e "\e[31m ---------------      Testing archive:      --------------- \e[0m"
unzip -t google_transit.zip # -t test archive


echo -e "\e[31m ---------------    Un-zipping to folder:   --------------- \e[0m"
unzip -o -u google_transit.zip -d google_transit #-o overwrites with no prompt, -u update existing files
rm google_transit.zip


echo -e "\e[31m ---------------   Running busDataBuilder:  --------------- \e[0m"
./busdatabuilder
builderStatus=$?
echo -n "BusDataBuilder exited with status: $builderStatus" | sendxmpp --file /home/colin/.sendxmpprc --tls colinl@momi.ca
echo "BusDataBuilder exited with status: $builderStatus"

if test $builderStatus -eq 0
then
echo -e "\e[31m ---------------         Cleanup:           --------------- \e[0m"
echo -n "The following routes are no-longer present in GTFS file:" | sendxmpp --file /home/colin/.sendxmpprc --tls colinl@momi.ca
echo The following routes are no longer present in GTFS, and will be deleted:
find prevTrips/* -mtime +0.2 | sendxmpp --file /home/colin/.sendxmpprc --tls colinl@momi.ca
find prevTrips/* -mtime +0.2 -exec rm {} \;


echo -e "\e[31m ---------------         All done!          --------------- \e[0m"
echo -n "PrevTrips GTFS data update complete." | sendxmpp --file /home/colin/.sendxmpprc --tls colinl@momi.ca

else

echo -e "\e[31m ---------------   busDataBuilder failed    --------------- \e[0m"
fi
