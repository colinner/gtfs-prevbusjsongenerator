/* 
 * File:   main.cpp
 * Author: Colin Leitner
 * Created on September 3, 2019, 4:16 PM
 * 
 ***** License:
 * This program is open-source software, but you must alert the original author if you plan to use it, modify it, or copy it.
 * Otherwise, it is under the terms of the GNU General Public License V3. See <http://www.gnu.org/licenses/>
 * This program is distributed WITHOUT ANY WARRANTY whatsoever.
 * 
 ***** About:
 * This source file works, but admittedly has some bad programming practices in it, mostly indicated by TODOs.
 * It is like this because I wanted to write it quickly and have no intention to make it polished.
 * Specifically, structs should be used rather than parallel arrays to load the GTFS database,
 * and searcher/get() functions should be used rather than about a dozen nested loops.
 * Some cout blocks could also be functions, and perhaps also loading each file could be generalized.
 *
 ***** How to use:
 * Run this program in the same directory as the google_transit folder,
 * and it will spit out a file for each weekday+route+direction, with filenames that will be requested by the HyperBus JS app.
 * The output folder "prevTrips" must already exist.
 */

#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <ctime> //for getting current date to check what schedule is currently in effect
#include <math.h> //for floor()
#include <iomanip> //for setw()

using namespace std;

string* dataRoutesName = new string[1000]; //a route number is max 3 digits (chars)
unsigned long dataRoutesSize = 0;

bool haveRoute(string rtName) { //simply check if the route name is already in the list
	for(unsigned long index = 0; index < dataRoutesSize; index++) {
		if(dataRoutesName[index] == rtName) {
			return true;
		}
	}
	return false;
}

long parseTime(string inTime) { //converts a time string to meaningless integer for comparison ONLY
	if(inTime.length() < 5) { cout << "\nInvalid time: '" << inTime << "'\n"; } //invalid time

	inTime.erase(inTime.find_first_of(':', 0), 1); //this will throw exception if no ":"
	inTime.erase(inTime.find_first_of(':', 0), 1);
	//turn into integer:
	long tempVal = strtol(inTime.c_str(), NULL, 10);
	return tempVal;
}

//function that can search a given array of any type
//starts at 'hint' position, search wraps to top and continues down if not found
//returns index of found data, or -1 if not found
template <typename arrayDataType>
long lookup(arrayDataType arrayIn[], arrayDataType findValue, unsigned long arraySize, long i_StartAt) {
	//search down from hint, then up from hint if not found?? Depends on how data is sorted
	//having two separate search loops is faster than one multi-purpose loop
	long i = i_StartAt;
	while((arrayIn[i] != findValue) && (i < arraySize)) {
		i++;
	}
	if(i == arraySize) { //didn't find value after hint
		//searching backwards will prevent returning all possible matches in arrayIn[], it will get stuck on last match and not loop back to index 0
		
		//re-start searching from 0 to i_StartAt:
		i = 0; //don't subtract 1 because while loop needs to check at this index
		cout << "Search wrapped while looking for " << findValue << endl;
		while((arrayIn[i] != findValue) && (i < i_StartAt)) {
			i++;
		}
		if(i == i_StartAt) {
			return -1; //entire array searched, not found
		}
		else {
			return i;
		}
	}
	else {
		return i;
	}
}

//function that completely handles parsing of any CSV file, and loads select columns into a psuedo 2D array [a*b], not a real [][] because it requires constant size apparently
//calling code must: provide pointer to store dataSize, and delete[] returned array after use
//file requirements:
//any whitespace between commas is captured
//trailing comma in header okay (causes extra field, slower)
//extra trailing commas in records okay (ignored)
//truncated records okay, causes empty strings for each missing cell
//linebreak after last record optional, no effect. However, any chars after will count as more records


//unsigned long readCSVtoArrays(string fileName, string colNamesIn[], string arrayOut[], unsigned int outColsN) {
//Originally, the plan was to return the size and store the array pointer to a referenced varaible, as above
//This did not work however, possibly because it's not possible for calling code to create a dynamic array, and then the function sets the size
string* readCSVtoArray(string fileName, string colNamesIn[], unsigned long &dataSize, unsigned int outColsN) {
	cout << "Loading CSV file: " << fileName << endl;
	
	ifstream dataFileIn(fileName);
	if (dataFileIn.fail()) {
		cout << "\033[31mLoading failed. Exiting.\033[0m\n";
		exit(1);
	}
	string currentLine;
	int totalCols = 0;
	
	//dataFileTrips.unsetf(std::ios_base::skipws); //new lines will be skipped by default?? This was suggested by stackoverflow answer, but seems to make no difference
	
	//determine column to array mapping:
	int colIisArrayX[1024]; //index/elements of this array == columns in CSV input, values of elements == index of corresponding colNamesIn[]
	//initialize array:
	for(int i=0; i<1024; i+=1) {
		colIisArrayX[i] = -1;
	}
	getline(dataFileIn, currentLine); //load header string
	cout << "Header: " << currentLine << endl;
	
	//cleanup:
	while(currentLine.back()=='\r') { //if Windows carriage return char
		currentLine.pop_back();
	}
	while(((long)currentLine.front() < ' ') || ((long)currentLine.front() > '~')) { //lately, has been weird chars appended to csv file, prevents field match later
		cout << "WARNING: removing non-ASCII character from start of header: #" << (long)currentLine.front() << endl;
		currentLine.erase(0,1); //clip first byte
	}
	
	cout << "Mapping requested fields:\n";
	int col = 0;
	int colFoundCounter = 0;
	while(currentLine.length() > 0) { //while there are still chars left (they are erased as parsed)
		string thisColName;
		int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
		if(nextComma > -1) {
			thisColName	= currentLine.substr(0, nextComma);
			currentLine.erase(0, nextComma + 1); //move to next field
		}
		else {
			thisColName = currentLine;
			currentLine = ""; //exit while loop
		}
		
	//	cout << "Checking if '" << thisColName << "' field requested..." << endl;
	//	cout << "'" << thisColName.front() << "' '" << thisColName.back() << "' \t";
	//	cout << endl;
		
		for(int i=0; i<outColsN; i+=1) {
		//	cout << "length=" << colNamesIn[i].length() << " \t";
		//	cout << "'" << colNamesIn[i].front() << "' '" << colNamesIn[i].back() << "' \t";
		//	cout << "cmp=" << thisColName.compare(colNamesIn[i]) << "\t";
			if(thisColName == colNamesIn[i]) {
				colIisArrayX[col] = i;
				cout << i << "-" << col << "\t" << thisColName << endl;
				colFoundCounter += 1;
			}
		}
	//	cout << endl;
		col += 1;
	}
	totalCols = col;
	cout << "File contains " << totalCols << " fields" << endl;
	
	//check that all requested columns were found:
	if(colFoundCounter < outColsN) {
		cout << "Error: " << (outColsN-colFoundCounter) << " field headers could not be found in file";
		exit(1);
	}
	
	//determine number of rows:
	dataSize = 0;
	//header has already been skipped above
	while (getline(dataFileIn, currentLine)) {
		dataSize++;
	}
	cout << "File contains " << dataSize << " records" << endl;
	
	//temporary array for loading data into:
	string* arrayOut = new string[dataSize * outColsN]; //real 2D array requires constant size???? :(
	
	//begin reading file, storing data into arrayTmp:
	dataFileIn.clear(); //reset "eof" flag to allow seeking
	dataFileIn.seekg(0, dataFileIn.beg); //go back to start
	getline(dataFileIn, currentLine); //skip file header
	
	unsigned long dataIndex = 0;
	while (dataIndex < dataSize) {
		getline(dataFileIn, currentLine);
		while(currentLine.back()=='\r') { //if Windows carriage return char
			currentLine.pop_back();
		}
		col = 0;
		while(col < totalCols) {
			string cellContent;
			int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
			if(nextComma > -1) { //if no match is found, it returns string::npos
				cellContent = currentLine.substr(0, nextComma); //extract upto next comma
				currentLine.erase(0, nextComma + 1); //move to next field
			}
			else {
				cellContent = currentLine;
				//this should be the last column if no more commas follow
			}
		//	cout << "'" << cellContent << "'\t";
			if(colIisArrayX[col] != -1) {
				arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] = cellContent;
	//	cout << "Stored=" << arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] << "\t";
			}
			
			col++;
		}
	//	cout << endl;
		dataIndex++;
	}
	
	dataFileIn.close();
	cout << "Done loading all records in file.\n";
	
	return arrayOut; //return pointer to newly created array
	//number of records found in file, not including header, is already stored in variable passed-in by ref
}

//copy of above, but stores values into int
//only change is that ":" chars are removed (like from 24hr time strings)
int* readCSVtoArrayInt(string fileName, string colNamesIn[], unsigned long &dataSize, unsigned int outColsN) {
	cout << "Loading CSV file: " << fileName << endl;
	
	ifstream dataFileIn(fileName);
	if (dataFileIn.fail()) {
		cout << "\033[31mLoading failed. Exiting.\033[0m\n";
		exit(1);
	}
	string currentLine;
	int totalCols = 0;
	
	//dataFileTrips.unsetf(std::ios_base::skipws); //new lines will be skipped by default?? This was suggested by stackoverflow answer, but seems to make no difference
	
	//determine column to array mapping:
	int colIisArrayX[1024];
	//initialize array:
	for(int i=0; i<1024; i+=1) {
		colIisArrayX[i] = -1;
	}
	getline(dataFileIn, currentLine);
	cout << "Header: " << currentLine << endl;
	
	//cleanup:
	while(currentLine.back()=='\r') { //if Windows carriage return char
		currentLine.pop_back();
	}
	while(((long)currentLine.front() < ' ') || ((long)currentLine.front() > '~')) { //lately, has been weird chars appended to csv file, prevents field match later
		cout << "WARNING: removing non-ASCII character from start of header: #" << (long)currentLine.front() << endl;
		currentLine.erase(0,1); //clip first byte
	}
	
	cout << "Mapping requested fields:\n";
	int col = 0;
	int colFoundCounter = 0;
	while(currentLine.length() > 0) { //while there are still chars left (they are erased as parsed)
		string thisColName;
		int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
		if(nextComma > -1) {
			thisColName	= currentLine.substr(0, nextComma);
			currentLine.erase(0, nextComma + 1); //move to next field
		}
		else {
			thisColName = currentLine;
			currentLine = ""; //exit while loop
		}
		
		for(int i=0; i<outColsN; i+=1) {
			if(thisColName == colNamesIn[i]) {
				colIisArrayX[col] = i;
				cout << i << "-" << col << "\t" << thisColName << endl;
				colFoundCounter += 1;
			}
		}
		col += 1;
	}
	totalCols = col;
	cout << "File contains " << totalCols << " fields" << endl;
	
	//check that all requested columns were found:
	if(colFoundCounter < outColsN) {
		cout << "Error: " << (outColsN-colFoundCounter) << " field headers could not be found in file";
		exit(1);
	}
	
	//determine number of rows:
	dataSize = 0;
	//header has already been skipped above
	while (getline(dataFileIn, currentLine)) {
		dataSize++;
	}
	cout << "File contains " << dataSize << " records" << endl;
	
	//temporary array for loading data into:
	int* arrayOut = new int[dataSize * outColsN]; //real 2D array requires constant size???? :(
	
	//begin reading file, storing data into arrayTmp:
	dataFileIn.clear(); //reset "eof" flag to allow seeking
	dataFileIn.seekg(0, dataFileIn.beg); //go back to start
	getline(dataFileIn, currentLine); //skip file header
	
	unsigned long dataIndex = 0;
	while (dataIndex < dataSize) {
		getline(dataFileIn, currentLine);
		while(currentLine.back()=='\r') { //if Windows carriage return char
			currentLine.pop_back();
		}
		col = 0;
		while(col < totalCols) {
			string cellContent;
			int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
			if(nextComma > -1) { //if no match is found, it returns string::npos
				cellContent = currentLine.substr(0, nextComma); //extract upto next comma
				currentLine.erase(0, nextComma + 1); //move to next field
			}
			else {
				cellContent = currentLine;
				//this should be the last column if no more commas follow
			}
			
			//remove colons of time cells for compression into an int:
			nextComma = cellContent.find_first_of(':', 0); //reuse variable from above for searching for colons
			while(nextComma != string::npos) {
				cellContent.erase(nextComma, 1);
				nextComma = cellContent.find_first_of(':', 0);
			}
			
			if(colIisArrayX[col] != -1) {
				arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] = strtol(cellContent.c_str(), NULL, 10);
	//	cout << arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] << "\t";
			}
			
			col++;
		}
	//	cout << endl;
		dataIndex++;
	}
	
	dataFileIn.close();
	cout << "Done loading all records in file.\n";
	
	return arrayOut; //return pointer to newly created array
	//number of records found in file, not including header, is already stored in variable passed-in by ref
}

int main(int argc, char** argv) {
	cout << "Starting...\n";
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//"stops.txt": Just for resolving the stop_code from stop_id
	
	cout << "Loading stop locations...\n";
	
	unsigned long stopLocDataSize;
	unsigned int dataStopLocs_ColsN = 2;
	string dataStopLocs_Cols[] = {"stop_id", "stop_code"};
	
	string* dataStopLocsExtracted = readCSVtoArray("google_transit/stops.txt", dataStopLocs_Cols, stopLocDataSize, dataStopLocs_ColsN);
	
	//allocate parallel arrays to store contents of file:
	unsigned int* dataStopLoc_Stop_id = new unsigned int[stopLocDataSize];
	unsigned int* dataStopLoc_StopCode = new unsigned int[stopLocDataSize];
	
	//copy into parallel arrays, converting datatypes:
	for(unsigned long dataStopLoc_i = 0; dataStopLoc_i < stopLocDataSize; dataStopLoc_i++) {
		dataStopLoc_Stop_id [dataStopLoc_i] = strtol(dataStopLocsExtracted[(dataStopLoc_i*dataStopLocs_ColsN)+0].c_str(), NULL, 10);
		dataStopLoc_StopCode[dataStopLoc_i] = strtol(dataStopLocsExtracted[(dataStopLoc_i*dataStopLocs_ColsN)+1].c_str(), NULL, 10);
		
	//	cout << dataStopLoc_StopCode[dataStopLoc_i] << "\t";
	//	cout << dataStopLoc_Stop_id [dataStopLoc_i] << endl;
	}
	
	delete[] dataStopLocsExtracted;
	cout << "Loaded " << stopLocDataSize << " stop locations from stops.txt\n\n";
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Load "calendar.txt" file to determine what service_id each weekday is.
	// There are usually multiple time periods in the same file,
	// and latest date range must be used for each weekday
	
	cout << "Loading service calendar...\n"; 
	
	unsigned long dataCal_Size;
	unsigned int dataCal_ColsN = 6;
	string dataCal_Cols[] = {"service_id","start_date","end_date","wednesday","saturday","sunday"};
	
	string* dataCalExtracted = readCSVtoArray("google_transit/calendar.txt", dataCal_Cols, dataCal_Size, dataCal_ColsN);
	
	const string servCode[] = { //these constant codes are used for filename output
	"WK",
	"SA",
	"SU" };
	
	//service_id to filter for each weekday:
	// the following strings are examples and overwritten, comment search code below to manual override
	string goodServ[] = {
	"1", //weekday service
	"2", //saturday
	"3" }; //sunday
	
	time_t now = time(0); // current date/time based on current system
	tm *ltm = localtime(&now);
	
	unsigned long todayDateCode = 19000000; //initialize to "time zero"
	todayDateCode += (ltm->tm_year) * 10000; //years since 1900
	todayDateCode += (1 + ltm->tm_mon) * 100; //0-indexed month of year
	todayDateCode += (ltm->tm_mday); //1-indexed day of month
	cout << "Today's date code to look for in-service trips: " << todayDateCode << endl;
	
	//find service_id's with latest start date, and also earlier than or equal to today's date:
	unsigned long highestStartDate[] = {0,0,0};
	unsigned long endDate[] = {0,0,0};
	
	for(unsigned long dataCal_i = 0; dataCal_i < dataCal_Size; dataCal_i++) { //search through calendar data entries
		for(unsigned int servDay = 0; servDay < 3; servDay++) { //attempt to match against each service type
			if(dataCalExtracted[(dataCal_i*dataCal_ColsN)+(3+servDay)] == "1") { //if this service is active on this day type
		//		cout << "dataCalExtracted=" << dataCalExtracted[(dataCal_i*dataCal_ColsN)+0] << endl;
				unsigned long dateParse = strtol(dataCalExtracted[(dataCal_i*dataCal_ColsN)+1].c_str(), NULL, 10);
		//		cout << "temp dateParse=" << dateParse << endl;
				if(dateParse > highestStartDate[servDay]) {
					if(dateParse <= todayDateCode) {
						goodServ[servDay] = dataCalExtracted[(dataCal_i*dataCal_ColsN)+0];
						highestStartDate[servDay] = dateParse;
						endDate[servDay] = strtol(dataCalExtracted[(dataCal_i*dataCal_ColsN)+2].c_str(), NULL, 10);
						cout << "Now using servDay: " << servCode[servDay] << " service starting: " << dateParse << " until: " << endDate[servDay] << ". goodServ= " << goodServ[servDay] << " (dataCal_i=" << dataCal_i << ")" << endl;
					}
				}
			}
		}
	}
	
	
	delete[] dataCalExtracted;
	cout << endl;
	
	//-------------------------------------------------------------------------------
	// File "trips.txt": Shows the hierarchy of block_id > trip_id, although trips are not in chrono order
	cout << "Loading trips and building route list...\n";
	
	unsigned long tripDataSize; //value will be written by readCSVtoArray()
	unsigned int tripData_ColsN = 5;
	string tripData_Cols[] = {"block_id", "trip_headsign", "direction_id", "service_id", "trip_id"};
	
	string* dataTripExtracted = readCSVtoArray("google_transit/trips.txt", tripData_Cols, tripDataSize, tripData_ColsN);
	
	//allocate parallel arrays to store contents of file:
	string* dataTrips_Block_id = new string[tripDataSize];
	char*   dataTrips_Direc = new char[tripDataSize];
	string* dataTrips_RtName = new string[tripDataSize];
	string* dataTrips_Serv_id = new string[tripDataSize];
	unsigned int* dataTrips_Trip_id = new unsigned int[tripDataSize];
	
	//parallel, but stored to later:
	int* dataTrips_StartTimeC = new int[tripDataSize]; //NEW! changed from string to integer!
	long* dataTrips_StartStopID = new long[tripDataSize];
	//string* dataTrips_EndTime = new string[tripDataSize];
	int*   dataTrips_EndTimeC = new int[tripDataSize]; //same as previous array, but string is pre-parsed to integer code for easy comparing
	long* dataTrips_EndStopID = new long[tripDataSize];
	long* dataTrips_SndStopID = new long[tripDataSize]; //the second-last stop ID
	//TODO structs for records!!! advantage of parallel arrays is that they can be searched separately
	
	//copy from 2D array into parallel arrays, converting datatypes:
	for(unsigned long dataTrips_i = 0; dataTrips_i < tripDataSize; dataTrips_i++) {
		dataTrips_Block_id[dataTrips_i] =		 dataTripExtracted[(dataTrips_i*tripData_ColsN)+0];
		dataTrips_Trip_id [dataTrips_i] = strtol(dataTripExtracted[(dataTrips_i*tripData_ColsN)+4].c_str(), NULL, 10);
		
		dataTrips_RtName  [dataTrips_i] =		 dataTripExtracted[(dataTrips_i*tripData_ColsN)+1].substr(0, dataTripExtracted[(dataTrips_i*tripData_ColsN)+1].find_first_of(' ', 0)); //only store upto first space
		
		if(dataTrips_RtName[dataTrips_i].length() > 0) {
		while(dataTrips_RtName[dataTrips_i].at(0) == '0') { dataTrips_RtName[dataTrips_i].erase(0, 1); } //remove any leading zeros from RtName
		}
		else {
			cout << "\033[31m Warning: trip_id " << dataTrips_Trip_id[dataTrips_i] << " has no route name \033[0m\n";
		}
		dataTrips_Direc   [dataTrips_i] =		 dataTripExtracted[(dataTrips_i*tripData_ColsN)+2].at(0);
		dataTrips_Serv_id [dataTrips_i] =		 dataTripExtracted[(dataTrips_i*tripData_ColsN)+3];
		
		//starting/ending times are left undefined until required file is loaded
		
		if(!haveRoute(dataTrips_RtName[dataTrips_i])) {
			dataRoutesName[dataRoutesSize] = dataTrips_RtName[dataTrips_i]; //add route to list
			cout << dataRoutesName[dataRoutesSize] << ", ";
			dataRoutesSize++;
		}
	}
	cout << endl;
	
	delete[] dataTripExtracted;
	delete[] dataTrips_Direc;
	cout << "Found " << dataRoutesSize << " unique routes in trips.txt\n\n";
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	//Next file, "stop_times.txt": this file is used to determine what time each trip occurs, and both the start and end stops
	cout << "Loading stop_times...\n";
	
	unsigned long dataStops_Size;
	unsigned int dataStops_ColsN = 4;
	string dataStops_Cols[] = {"trip_id", "departure_time", "stop_id", "stop_sequence", };
	
	int* dataStopsExtracted = readCSVtoArrayInt("google_transit/stop_times.txt", dataStops_Cols, dataStops_Size, dataStops_ColsN);
	
	//allocate parallel arrays to store contents of file. The first group of 4 is temporary:
	unsigned int* dataStops_Trip_id =		new unsigned int[dataStops_Size]; //8 digit number
	unsigned int* dataStops_DepartTimeC =	new unsigned int[dataStops_Size]; //NEW!! changed to int!
	unsigned int* dataStops_Stop_id =		new unsigned int[dataStops_Size];
	unsigned short* dataStops_Seq =			new unsigned short[dataStops_Size];
	
	//copy into parallel arrays, converting datatypes:
	for(unsigned long dataStops_i = 0; dataStops_i < dataStops_Size; dataStops_i++) {
		dataStops_Trip_id    [dataStops_i] = dataStopsExtracted[(dataStops_i*dataStops_ColsN)+0];
		dataStops_DepartTimeC[dataStops_i] = dataStopsExtracted[(dataStops_i*dataStops_ColsN)+1];
		dataStops_Stop_id    [dataStops_i] = dataStopsExtracted[(dataStops_i*dataStops_ColsN)+2];
		dataStops_Seq        [dataStops_i] = dataStopsExtracted[(dataStops_i*dataStops_ColsN)+3];
	}
	
	delete[] dataStopsExtracted;
	cout << "Loaded " << dataStops_Size << " stop events from stop_times.txt\n\n";
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	// Go through trips table again, and look up and copy over both trip START and END time from the trip's stops in dataStops_*:
	// dataStops_* does not have to be grouped by trip_id, or sorted by _Seq, but it makes this section faster
	//this section takes so long because dataStops is searched multiple times, the largest array
	
	const int maxStopsInAnyTrip = 100;
	
	cout << "Determining trip endpoint times and stop codes...\n";
	long dataStops_i = 0; //this is persistent to provide search algorithm with starting position hints
	for(unsigned long dataTrips_i = 0; dataTrips_i < tripDataSize; dataTrips_i++) {
		if((dataTrips_Serv_id[dataTrips_i]==goodServ[0])||
			(dataTrips_Serv_id[dataTrips_i]==goodServ[1])||
			(dataTrips_Serv_id[dataTrips_i]==goodServ[2])) { //only fill in data for trips that are in service
		
		//look up stop in this trip with _Seq == 1:
		
		/*
		//old simple search for tripID with seq=1
		//NOTE should not be needed, quick search has been reviewed and seems robust
		dataStops_i = 0;
		while (((dataStops_Trip_id[dataStops_i] != dataTrips_Trip_id[dataTrips_i]) || (dataStops_Seq[dataStops_i] != 1)) && (dataStops_i < dataStops_Size)) {
			dataStops_i++;
		}
		if(dataStops_i == dataStops_Size) { //searched through entire array, not found
			cout << "GTFS ERROR: Can't find start time for trip_id " << dataTrips_Trip_id[dataTrips_i] << " in stop_times.txt. Either: file does not contain trip, or trip missing #1 stop sequence.\n";
		//	cout << "Writing -1 to dataTrips_[] table for this trip.\n";
			dataTrips_StartTimeC[dataTrips_i] = -1;
			dataTrips_StartStopID[dataTrips_i] = -1;
		}
		else { //copy over trip START time and info:
			dataTrips_StartTimeC[dataTrips_i] = dataStops_DepartTimeC[dataStops_i];
			dataTrips_StartStopID[dataTrips_i] =  dataStops_Stop_id[dataStops_i];
		}
		*/
		
		
		long dataStops_i_LowestRecordOfTrip = -1;
		
		//new quick search: (achieves same thing as code block above)
		bool found = false;
		long failCount = 0;
		while(!found) { //while not found, keep repeating search for certain stop of trip until seq == 1
		//	cout << "Starting search at stopTime index " << dataStops_i << endl;
			dataStops_i = lookup(dataStops_Trip_id, dataTrips_Trip_id[dataTrips_i], dataStops_Size, dataStops_i);
			if(dataStops_i == -1) { //must check this first to avoid seg fault
				cout << "Can't find trip_id " << dataTrips_Trip_id[dataTrips_i] << " in stop_times.txt.\n";
				exit(1);
			}
			else if(dataStops_Seq[dataStops_i] == 1) { //check if this stop is the first in the trip
				//search complete, copy over trip START time and info:
				dataTrips_StartTimeC[dataTrips_i] = dataStops_DepartTimeC[dataStops_i];
				dataTrips_StartStopID[dataTrips_i] =  dataStops_Stop_id[dataStops_i];
				found = true;
			//	cout << "Found first stop for route " << dataTrips_RtName[dataTrips_i] << " trip_id " << dataTrips_Trip_id[dataTrips_i] << endl;
			}
			else {
				//will get stuck here if seq==1 doesn't exist
				failCount++;
				if(failCount > maxStopsInAnyTrip) {
					cout << "GTFS ERROR: Trip " << dataTrips_Trip_id[dataTrips_i] << " may have too many stops (try sorting stop_times.txt by trip_id?), trip not in stop_times.txt, or no trip seq#1." << endl;
				//	cout << "Writing -1 to dataTrips_[] table for this trip.\n";
					dataTrips_StartTimeC[dataTrips_i] = -1;
					dataTrips_StartStopID[dataTrips_i] = -1;
					found = true;
				}
			}
			if(dataStops_i_LowestRecordOfTrip == -1) {
				dataStops_i_LowestRecordOfTrip = dataStops_i; //remember location for next trip search
			}
			dataStops_i++; //stop event at this index already processed, starting search here again will cause infinite loop
			if(dataStops_i >= dataStops_Size) {
				dataStops_i = 0;
			}
		}
		
		
		if(dataStops_i_LowestRecordOfTrip < 0) { dataStops_i_LowestRecordOfTrip = 0; } //in case for some reason this was not set by search above (like if code disabled)
		
		//search for highest _Seq to use as ending stop:
		//dataStops_i = dataStops_i_LowestRecordOfTrip; //continue search at lowest known record of current trip, allows for completely unsorted data
		dataStops_i = 0; //NOTE this search for highest seq must search all records. Continuing is only possible if the data is grouped by trip_id, which is a big assumption
		unsigned long dataStops_i_Highest = 0;
		unsigned long dataStops_i_2ndHighest = 0;
		long dataStops_TopSeqInTrip = 1;
	//	cout << dataStops_i << " " << dataStops_Size << endl;
		while (dataStops_i < dataStops_Size) {
			if(dataStops_Trip_id[dataStops_i] == dataTrips_Trip_id[dataTrips_i]) { //check if this stopTime is part of the same trip
				if(dataStops_Seq[dataStops_i] > dataStops_TopSeqInTrip) { //check if the sequence is higher
				//	cout << "Found higher seq:" << dataStops_Seq[dataStops_i] << endl;
					dataStops_i_2ndHighest = dataStops_i_Highest;
					dataStops_TopSeqInTrip = dataStops_Seq[dataStops_i]; //store new best
					dataStops_i_Highest = dataStops_i;
				}
			}
			dataStops_i++;
		}
		if(dataStops_TopSeqInTrip == 1) { //searched through entire array, no improvement
			cout << "Can't find end stop for trip_id " << dataTrips_Trip_id[dataTrips_i] << " in dataStops[]\n";
			exit(1);
		}
		else if(dataStops_TopSeqInTrip == 2) {
			cout << "Warning: Only 2 stops for trip_id " << dataTrips_Trip_id[dataTrips_i] << ", route " << dataTrips_RtName[dataTrips_i] << endl;
			dataStops_i_2ndHighest = dataStops_i_Highest;
		}
		
		//copy over trip END time and info:
		dataTrips_EndTimeC[dataTrips_i] =			dataStops_DepartTimeC[dataStops_i_Highest]; //NEW used to be string
		dataTrips_EndStopID[dataTrips_i] =             dataStops_Stop_id[dataStops_i_Highest];		
		dataTrips_SndStopID[dataTrips_i] =          dataStops_Stop_id[dataStops_i_2ndHighest];
		
		
		/*
		cout << dataTrips_i << "\t";
		cout << dataTrips_Block_id[dataTrips_i] << "\t";
	//	cout << dataTrips_Direc[dataTrips_i] << "\t"; // array memory freed above!!
		cout << dataTrips_RtName[dataTrips_i] << "\t";
		cout << dataTrips_Serv_id[dataTrips_i] << "\t\t";
		cout << dataTrips_Trip_id[dataTrips_i] << "\t";
		cout << dataTrips_StartTimeC[dataTrips_i] << " - " << dataTrips_EndTimeC[dataTrips_i] << "\t";
		cout << dataTrips_StartStopID[dataTrips_i] << " - " << dataTrips_EndStopID[dataTrips_i] << " / " << dataTrips_SndStopID[dataTrips_i];
		cout << endl;
		*/
		
		dataStops_i = dataStops_i_LowestRecordOfTrip; //restore search index for faster finding next trip
		}
		
		//infrequent milestone output so program doesn't appear dead:
		if(dataTrips_i % 10000 == 0) {
			cout << "Passed " << dataTrips_i << "th trip: " << dataTrips_Trip_id[dataTrips_i] << endl;
		}
	}
	cout << endl;
	
	delete[] dataStops_Trip_id;
	delete[] dataStops_DepartTimeC;
	delete[] dataStops_Stop_id;
	delete[] dataStops_Seq;
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	//TODO this section could be combined with the above for() iterating dataTrips
	
	//determine all starting stops for each route, to determine how many files to create per route:
	const int maxStartPts = 10; //if more are found, they are ignored
	
	//jagged array where first index is route, 2nd is stop_id of starting point, first element of each row (subarrray) is the number of starting points (length of subarray)
	//this array is an extension of dataRoutesName[]
	unsigned long (*dataStartPts)[maxStartPts+1] = new unsigned long[dataRoutesSize][maxStartPts+1];
	
	//initialize count to 0 for all routes:
	for(long dataStartPts_Rti = 0; dataStartPts_Rti < dataRoutesSize; dataStartPts_Rti++) {
		dataStartPts[dataStartPts_Rti][0] = 0;
	}
	
	//scan all trips, looking for unique starting stop_ids
	for(unsigned long dataTrips_i = 0; dataTrips_i < tripDataSize; dataTrips_i++) {
		if(((dataTrips_Serv_id[dataTrips_i] == goodServ[0])||
			(dataTrips_Serv_id[dataTrips_i] == goodServ[1])||
			(dataTrips_Serv_id[dataTrips_i] == goodServ[2]))&&
			(dataTrips_StartStopID[dataTrips_i] != -1)) { //a trip's stopID gets set to -1 if GTFS data bad, see above. Just skip this trip
		
		//find array index of route for this trip:
		unsigned long dataRoutes_i = 0;
		while((dataRoutes_i < dataRoutesSize) && (dataRoutesName[dataRoutes_i] != dataTrips_RtName[dataTrips_i])) {
			dataRoutes_i++;
		}
		//TODO fairly safe to assume that record is found, since data is derived from same source
		//check if the starting stop_id is already known:
		
		int dataStartPts_Stpi = 0; //this is the stop index, not actually the array index, note the +1's
		while((dataStartPts_Stpi < dataStartPts[dataRoutes_i][0]) && (dataStartPts[dataRoutes_i][dataStartPts_Stpi+1] != dataTrips_StartStopID[dataTrips_i])) {
			dataStartPts_Stpi++;
		}
		if(dataStartPts_Stpi == dataStartPts[dataRoutes_i][0]) { //if all existing stops don't match
			if(dataStartPts_Stpi > (maxStartPts-1)) { //there is no more room to store another starting point TODO not sure if -1
				cout << "\033[31m Warning: too many starting points for route " << dataRoutesName[dataRoutes_i] << "\033[0m\n";
			}
			else {
				dataStartPts[dataRoutes_i][dataStartPts_Stpi+1] = dataTrips_StartStopID[dataTrips_i];
				dataStartPts[dataRoutes_i][0]++;
			}
			cout << "Route " << dataRoutesName[dataRoutes_i] << " \t starts at stop_id " << dataTrips_StartStopID[dataTrips_i] << endl;
		}
		else {
			//stop already exists in array
		}
		
		}
	}
	cout << endl;
	
	//output jagged array contents for debug:
	for(unsigned long dataRoutes_i = 0; dataRoutes_i < dataRoutesSize; dataRoutes_i++) {
		cout << dataRoutesName[dataRoutes_i] << ": \t";
		for(int dataStartPts_Stpi = 0; dataStartPts_Stpi < dataStartPts[dataRoutes_i][0]; dataStartPts_Stpi++) {
			cout << dataStartPts[dataRoutes_i][dataStartPts_Stpi+1] << ", ";
		}
		cout << endl;
	}
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//Now that all data loaded, begin outputting files:
	//TODO currently, routes that don't show their # in trips.txt will have a vaild but non-conforming filename, not accessible from javaScript app
	
	ofstream indexFile;
	indexFile.open("prevTrips/index.html", (ios::out)); //"ios::app" is not used here, so that content is replaced
	if(!indexFile.is_open()) {
		cout << "\nCould not create file. Maybe prevTrips folder missing? \n";
		exit(1);
	}
	indexFile << "<html><head><title>JSON Bus Route Index</title></head><body>" << endl;
	indexFile << "<p>Last Updated: " << todayDateCode << "</p>" << endl;
	
	//I can't believe there are people doing some prayer behind me, that's crazy
	//I am freaking out!!
	//wow
	
	//begin building route connection tables:
	for(unsigned long routeIndex = 0; routeIndex < dataRoutesSize; routeIndex++) { //TODO change var name
		//for(char routeDirec = '0'; routeDirec < '2'; routeDirec++) { //TODO change to starting stop
		for(int dataStartPts_Stpi = 0; dataStartPts_Stpi < dataStartPts[routeIndex][0]; dataStartPts_Stpi++) {
			for(int servTodo = 0; servTodo < 3; servTodo++) { //loop for each WK,SA,SU days
				if(dataStartPts[routeIndex][0] == 0) { //if no starting points
					cout << "\033[31m Skipping file! Can't find any starting points for route '" << dataRoutesName[routeIndex] << "', servID: " << goodServ[servTodo] << "\033[0m\n";
					indexFile << "No trips found for route '" << dataRoutesName[routeIndex] << "' on day: " << servCode[servTodo] << "<br>\n";
				}
				else {
					long routeStartingStop = -1;
					
					//resolve stopId to stopCode:
					unsigned long StopIndex = 0;
					while((dataStopLoc_Stop_id[StopIndex] != dataStartPts[routeIndex][dataStartPts_Stpi+1]) && (StopIndex < stopLocDataSize)) {
						StopIndex++;
					}
					if(StopIndex == stopLocDataSize) {
						cout << "\nCan't find starting stopCode for stop_id " << dataStartPts[routeIndex][dataStartPts_Stpi+1] << ".\n";
						exit(1);
					}
					routeStartingStop = dataStopLoc_StopCode[StopIndex];
					
					
					string fileName = servCode[servTodo]+"_"+dataRoutesName[routeIndex]+"_"+to_string(routeStartingStop)+".json";
					ofstream outputFile;
					outputFile.open("prevTrips/"+fileName, (ios::out)); //"ios::app" is not used here
					if(!outputFile.is_open()) {
						cout << "Could not create file. Maybe prevTrips folder missing? or file is busy?\n";
						exit(1);
					}
					cout << "Created file: route " << dataRoutesName[routeIndex] << ", servID " << goodServ[servTodo] << "... ";
					
					indexFile << "<a href='" << fileName << "'>" << fileName << "</a><br>\n";
					
					outputFile << "{\"RouteNo\":\"" << dataRoutesName[routeIndex] << "\",\"Start\":" << routeStartingStop << ",\"Trips\":[" << endl;
					
					//How sorting works:
					//keep track of times done so far (start at 00:00), and best time found for current scan (start at max).
					//Scan whole table for record meeting criteria: time > already done, track lowest time record that matches.
					//when reaching end of scan, do that record (exec if branch), or abort file if can't find any more trips.
					//TODO this method is extremely slow: similar to "selection sort" but worse because it compares already sorted records

					//To handle duplicates: also track index of last done record.
					//If encountering a record that is equal time value, then it must be lower index.
					//would need to reset this index when search complete and result is not a duplicate.

					long timeCompleted = 0; //integer time code that has been output so far
			//		unsigned long indexCompleted = tripDataSize; //prevents selecting a duplicate time twice UNUSED, see how to above
					bool tripsRemaining = true; //flag for ending the while loop to end this route/dir file
					bool firstTrip = true; //for tracking whether to add a comma between trips
					while (tripsRemaining) {
						unsigned long nextBestIndex = 0;
						long nextBestTimeCode = 999999;
						
						for(unsigned long tripIndex = 0; tripIndex < tripDataSize; tripIndex++) { //scan through trip table for matching trips
							if(dataTrips_RtName[tripIndex] == dataRoutesName[routeIndex]) { //check if it matches
							if(dataTrips_Serv_id[tripIndex] == goodServ[servTodo]) {
							if(dataTrips_StartStopID[tripIndex] == dataStartPts[routeIndex][dataStartPts_Stpi+1]) {
							if(dataTrips_EndTimeC[tripIndex] > timeCompleted) {
								//trip is eligible to appear in table, now decide if it is next best:
								if(dataTrips_EndTimeC[tripIndex] < nextBestTimeCode) {
									nextBestIndex = tripIndex;
									nextBestTimeCode = dataTrips_EndTimeC[tripIndex];
								}
							}
							}
							}
							}
						}

						if(nextBestTimeCode == 999999) { //no sooner time code overwrote this value
							tripsRemaining = false;
						}
						else {
							//this trip matches criteria, now output some info about it:
							unsigned long tripIndex = nextBestIndex; //copy value from search above
							if (!firstTrip) { outputFile << ",\n"; } firstTrip = false;
							outputFile << "{";
						//	outputFile << dataTrips_RtName[tripIndex] << ",\t"; //TODO consider writing trip headsign
							outputFile << "\"b\":\"" << dataTrips_Block_id[tripIndex] << "\", ";
							outputFile << "\"t\":" << dataTrips_Trip_id[tripIndex] << ", ";
							unsigned long timeCtemp = dataTrips_StartTimeC[tripIndex];
							outputFile << setw(2) << setfill('0');
							outputFile << "\"s\":\"" << floor(timeCtemp/10000.0) << ":" << floor((timeCtemp % 10000)/100.0) << ":" << (timeCtemp % 100) << "\", ";
										  timeCtemp = dataTrips_EndTimeC[tripIndex];
							outputFile << "\"e\":\"" << floor(timeCtemp/10000.0) << ":" << floor((timeCtemp % 10000)/100.0) << ":" << (timeCtemp % 100) << "\", "; //not neccesary, future possibility for determining time to travel route
							outputFile << setw(0);
							
							outputFile << "\"p\":{"; //start of "previous trip" data element

							//search through all trips again for trip within same block_id, keeping track of time to beat:
							//long timeToBeat = parseTime(dataStopsEndP_DepartTime[stopTimeIndex]);
							long timeToBeat = dataTrips_EndTimeC[tripIndex]; //TODO why is this EndTime, not start???? because then only one time needs to be parsed?
							long long tripBest = -1; //no best yet
							for(unsigned long tripPrevIndex = 0; tripPrevIndex < tripDataSize; tripPrevIndex++) {
								if(dataTrips_Block_id[tripPrevIndex] == dataTrips_Block_id[tripIndex]) {//check if block_id matches
								if(dataTrips_Serv_id[tripPrevIndex] == goodServ[servTodo]) { //check that this trip is part of a valid service_id- it seems that blocks are not strictly children of service_ids, perhaps they are reused?
									
									//the trip at tripPrevIndex is in the same block and valid service, now check if it has competitive time:
									if(dataTrips_EndTimeC[tripPrevIndex] < timeToBeat) {
										//trip occurs before present trip, check if better than best so far:
									//	cout << " CAND=" << dataTrips_EndTime[tripPrevIndex];
										//compare current best to new candidate, if possible. if tripBest index -1, there is no best yet, so also store as best so far:
										if(tripBest == -1) {
											tripBest = tripPrevIndex;
										}
										else if(dataTrips_EndTimeC[tripPrevIndex] > dataTrips_EndTimeC[tripBest]) { //this can't be combined with above if() because it must not be evaluated if tripBest == -1 
											tripBest = tripPrevIndex;
										}
									}
								}
								else {
									cout << "Warning: block_id " << dataTrips_Block_id[tripIndex] << " contains trip " << dataTrips_Trip_id[tripPrevIndex] << " with deviant service_id: " << dataTrips_Serv_id[tripPrevIndex] << endl;
								}
								}
							}

							//check if tripBest was unchanged from present trip:
							if(tripBest == -1) {
								outputFile << "\"r\":false, \"msg\":\"None found! First in block?\"";
							}
							else {
								//tripBest now holds the index of prev trip! output the info for this trip:
								outputFile << "\"r\":true, ";
								outputFile << "\"t\":" << dataTrips_Trip_id[tripBest] << ", ";
								unsigned long endTimeC = dataTrips_EndTimeC[tripBest];
								outputFile << setw(2) << setfill('0');
								outputFile << "\"e\":\"" << floor(endTimeC/10000.0) << ":" << floor((endTimeC % 10000)/100.0) << ":" << (endTimeC % 100) << "\", ";
								outputFile << setw(0);
								outputFile << "\"n\":\"" << dataTrips_RtName[tripBest] << "\", ";

								//resolve stopId to code:
								unsigned long StopIndex = 0;
								while((dataStopLoc_Stop_id[StopIndex] != dataTrips_EndStopID[tripBest]) && (StopIndex < stopLocDataSize)) {
									StopIndex++;
								}
								if(StopIndex == stopLocDataSize) { cout << "\nCan't find ending stopCode for stop_id " << dataTrips_EndStopID[tripBest] << ".\n"; exit(1); } //NOTE this indexing var was previously "tripIndex"
								else {
									outputFile << "\"c\":" << dataStopLoc_StopCode[StopIndex] << ", ";
								}
								
								//resolve stopId to code:
								StopIndex = 0;
								while((dataStopLoc_Stop_id[StopIndex] != dataTrips_SndStopID[tripBest]) && (StopIndex < stopLocDataSize)) {
								//	cout << dataStopLoc_Stop_id[StopIndex] << ", ";
									StopIndex++;
								}
								if(StopIndex == stopLocDataSize) { cout << "\nCan't find ending stopCode for 2nd last stop_id " << dataTrips_SndStopID[tripBest] << " of trip " << tripBest << endl; exit(1); }
								else {
									outputFile << "\"c2\":" << dataStopLoc_StopCode[StopIndex];
								}
							}
							outputFile << "}}"; //end of "previous trip" data element and trip

							//set completed values for next search:
							timeCompleted = dataTrips_EndTimeC[tripIndex];
						}
					}
					outputFile << "\n]}\n";
					outputFile.close(); //done

					cout << "Done " << fileName << endl;
				}
			
		//	if(dataRoutesName[routeIndex] == "6") { routeIndex = dataRoutesSize; } //uncomment for testing only upto a certain route
			}
		}
	}
	
	indexFile << "</body>";
	indexFile.close();
	
	//cleanup all dynamic arrays:
	delete[] dataRoutesName;
	
	//delete[] dataStops_Trip_id;
	//delete[] dataStops_DepartTime;
	//delete[] dataStops_Stop_id;
	//delete[] dataStops_Seq;
	
	delete[] dataTrips_Block_id;
	//delete[] dataTrips_Direc;
	delete[] dataTrips_RtName;
	delete[] dataTrips_Serv_id;
	delete[] dataTrips_Trip_id;
	
	delete[] dataTrips_StartTimeC;
	delete[] dataTrips_StartStopID;
	//delete[] dataTrips_EndTime;
	delete[] dataTrips_EndTimeC;
	delete[] dataTrips_EndStopID;
	
	delete[] dataStopLoc_Stop_id;
	delete[] dataStopLoc_StopCode;
	
	delete[] dataStartPts;
	
	cout << "\nOutput complete. All memory freed. Have a nice trip :) \n";
	cout << "Dataset Summary:\n";
	cout << "service_id used:\n";
	for(int servTodo = 0; servTodo < 3; servTodo++) { //loop for each WK,SA,SU days
	cout << servCode[servTodo] << "\t" << goodServ[servTodo] << " \tValid from: " << highestStartDate[servTodo] << " to " << endDate[servTodo] << endl;
	}
	cout << "Known service_ids:" << dataCal_Size << endl;
	cout << "Unique routes:    " << dataRoutesSize << endl;
	cout << "Stop locations:   " << stopLocDataSize << endl;
	cout << "Stop events:      " << dataStops_Size << endl;
	
	return 0;
}
